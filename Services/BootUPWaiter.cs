﻿using DatalinkEngineering.CANopen;
using System;
using System.Collections.Concurrent;

namespace Services
{
	public class BootUpWaiter
	{
		#region Fields

		public static readonly ushort start_app_index = 0x1F51;
		public static readonly byte start_app_subindex = 1;
		public static readonly byte start_code = 1;

		private BlockingCollection<DateTime> bootupMsgs = new BlockingCollection<DateTime>();
		private NMT_Master_NET NMT_Master;

		#endregion Fields

		#region Constructors

		public BootUpWaiter(Factory factory, byte node_id)
		{
			NMT_Master = factory.buildNMTMaster();
			NMT_Master.registerNodeStateCallback((object obj, byte _node_id, byte state) =>
			{
				if (_node_id == node_id && state == 0)
				{
					bootupMsgs.Add(DateTime.Now);
				}
				return CanOpenStatus.CANOPEN_OK;
			}, null);
		}

		#endregion Constructors

		#region Methods

		public void Dispose()
		{
			NMT_Master.Dispose();
		}

		public void WaitBootup(int timeout_ms = 100)
		{
			if (!bootupMsgs.TryTake(out DateTime dt, timeout_ms))
			{
				throw new TimeoutException($"Не получено подтверждения успешной загрузки за {timeout_ms} мс");
			}
		}

		#endregion Methods
	}
}