﻿using DatalinkEngineering.CANopen;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Services
{
	namespace LSSFastScan
	{
		public class LSSFastScanScaner : IDisposable
		{
			#region Fields

			public static readonly byte CS_FAST_SCAN = 0x51;

			public static readonly byte CS_IDENTIFY_SLAVE = 0x4F;

			public static readonly uint LSS_RX_COBID = 0x7E4;

			public static readonly uint LSS_TX_COBID = 0x7E5;

			private static readonly int lss_wait_resp_ms = 10;

			private static readonly int RESPONSE_TIMEOUT_MS = 500;

			private List<Msg> awaitsEchoMsgs = new List<Msg>();

			private CanMonitor_NET monitor = new CanMonitor_NET();

			private BlockingCollection<byte[]> rxQueue = new BlockingCollection<byte[]>();

			#endregion Fields

			#region Delegates

			public delegate void ReportProgress(byte lss_bit_check, byte lss_sub);

			#endregion Delegates

			#region Methods

			public CanOpenStatus canHardwareConnect(int port, int btr)
			{
				var res = monitor.canHardwareConnect(port, btr);
				if (res == CanOpenStatus.CANOPEN_OK)
				{
					monitor.registerCanReceiveCallback(this, CanRxDelegate);
				}
				return res;
			}

			public CanOpenStatus canHardwareDisconnect() => monitor.canHardwareDisconnect();

			public void Dispose() => monitor.Dispose();

			public bool ResetFastScan() => send_fast_scan_message(0, 128, 0, 0);

			public bool FindDevice(out UniqueLSSId lss_id, ReportProgress reporter = null)
			{
				// perform fast scan
				// https://www.can-cia.org/fileadmin/resources/documents/proceedings/2008_pfeiffer.pdf
				// https://github.com/christiansandberg/canopen/blob/master/canopen/lss.py

				lss_id = new UniqueLSSId();
				byte lss_sub = 0;
				byte lss_next = 0;

				if (ResetFastScan())
				{
					Thread.Sleep(lss_wait_resp_ms);
					while (lss_sub < 4)
					{
						byte lss_bit_check = 32;
						while (lss_bit_check > 0)
						{
							--lss_bit_check;

							reporter?.Invoke(lss_bit_check, lss_sub);

							if (!send_fast_scan_message(lss_id[lss_sub], lss_bit_check, lss_sub, lss_next))
							{
								lss_id[lss_sub] |= 1u << lss_bit_check;
							}

							Thread.Sleep(lss_wait_resp_ms);
						}

						lss_next = (byte)((lss_sub + 1) & 3);

						if (!send_fast_scan_message(lss_id[lss_sub], lss_bit_check, lss_sub, lss_next))
						{
							return false;
						}

						Thread.Sleep(lss_wait_resp_ms);

						++lss_sub;
					}

					lss_id.KnownMask = UniqueLSSId.KnownBits.All;

					return true;
				}

				return false;
			}

			public bool FindDevicePartialyKnown(ref UniqueLSSId lss_id, ReportProgress reporter = null)
			{
				byte lss_sub = 0;
				byte lss_next = 0;

				if (ResetFastScan())
				{
					Thread.Sleep(lss_wait_resp_ms);
					while (lss_sub < 4)
					{
						if (!lss_id.IsSubKnown(lss_sub))
						{
							byte lss_bit_check = 32;
							while (lss_bit_check > 0)
							{
								--lss_bit_check;

								reporter?.Invoke(lss_bit_check, lss_sub);

								if (!send_fast_scan_message(lss_id[lss_sub], lss_bit_check, lss_sub, lss_next))
								{
									lss_id[lss_sub] |= 1u << lss_bit_check;
								}

								Thread.Sleep(lss_wait_resp_ms);
							}
						}

						lss_next = (byte)((lss_sub + 1) & 3);

						if (!send_fast_scan_message(lss_id[lss_sub], 0, lss_sub, lss_next))
						{
							return false;
						}

						Thread.Sleep(lss_wait_resp_ms);

						++lss_sub;
					}

					lss_id.KnownMask = UniqueLSSId.KnownBits.All;

					return true;
				}

				return false;
			}

			private static CanOpenStatus CanRxDelegate(object obj, uint id, byte[] data, byte dlc, uint flags)
			{
				LSSFastScanScaner _this = (LSSFastScanScaner)obj;

				var rxMsg = new Msg
				{
					id = id,
					data = data,
					dlc = dlc,
					flags = flags
				};

				var index = _this.awaitsEchoMsgs.FindIndex(m => m.Equals(rxMsg));
				if (index < 0)
				{
					_this.rxQueue.Add(data);
				}
				else
				{
					_this.awaitsEchoMsgs.RemoveAt(index);
				}

				return CanOpenStatus.CANOPEN_OK;
			}

			private bool send_command(byte[] msg, out byte[] result)
			{
				// clead rx queue
				while (rxQueue.Count != 0)
				{
					rxQueue.Take();
				}

				awaitsEchoMsgs.Add(new Msg
				{
					id = LSS_TX_COBID,
					data = msg,
					dlc = 8,
					flags = 0
				});
				monitor.canWrite(LSS_TX_COBID, msg, 8, 0);

				return rxQueue.TryTake(out result, RESPONSE_TIMEOUT_MS);
			}

			private bool send_fast_scan_message(uint id_number, byte bit_checker, byte lss_sub, byte lss_next)
			{
				var id_b = BitConverter.GetBytes(id_number);
				var messgae = new byte[] { CS_FAST_SCAN, 0, 0, 0, 0, bit_checker, lss_sub, lss_next };
				Array.Copy(id_b, 0, messgae, 1, id_b.Length);

				byte[] recv_msg = null;
				if (!send_command(messgae, out recv_msg))
				{
					return false;
				}

				return recv_msg[0] == CS_IDENTIFY_SLAVE;
			}

			#endregion Methods
		}

		internal class Msg
		{
			#region Fields

			public byte[] data;
			public byte dlc;
			public uint flags;
			public uint id;

			#endregion Fields

			#region Methods

			public bool Equals(Msg lr)
			{
				return lr.id == id &&
					lr.data.SequenceEqual(data) &&
					lr.dlc == dlc &&
					lr.flags == flags;
			}

			#endregion Methods
		}
	}
}