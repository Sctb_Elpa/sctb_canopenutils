﻿using DatalinkEngineering.CANopen;
using System.Linq;

namespace Services
{
	public class BootloaderSettingsAccessor
	{
		#region Fields

		public ushort settingsIndex = 0x1F5A;
		public byte settingsSubIndex = 1;

		private ClientSDO_NET clientSDO;

		#endregion Fields

		#region Constructors

		public BootloaderSettingsAccessor(ClientSDO_NET clientSDO)
		{
			this.clientSDO = clientSDO;
		}

		#endregion Constructors

		#region Methods

		public byte[] Load()
		{
			byte[] res = new byte[65536];

			var err = clientSDO.objectRead(settingsIndex, settingsSubIndex, res,
				out uint size, out uint errInfo);
			if (err != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось прочитать настройки", err);
			}

			return res.Take((int)size).ToArray();
		}

		public void Save(byte[] settings)
		{
			var crc = Utils.crc16_ccit_0(settings);

			var err = clientSDO.objectWriteBlock(settingsIndex, settingsSubIndex, crc, settings,
				(uint)settings.Length, out uint errInfo);

			if (err != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось записать настройки", err);
			}
		}

		#endregion Methods
	}
}