﻿using DatalinkEngineering.CANopen;
using System;
using System.Collections.Concurrent;
using System.IO;

namespace Services
{
	public class ApplicationStarter : IDisposable
	{
		#region Fields

		public static readonly ushort start_app_index = 0x1F51;
		public static readonly byte start_app_subindex = 1;
		public static readonly byte start_code = 1;

		private BlockingCollection<DateTime> bootupMsgs = new BlockingCollection<DateTime>();
		private ClientSDO_NET clientSDO;
		private BootUpWaiter bootUpWaiter;
		private byte node_id;

		#endregion Fields

		#region Constructors

		public ApplicationStarter(ClientSDO_NET clientSDO, Factory factory, byte node_id)
		{
			this.clientSDO = clientSDO;
			this.node_id = node_id;

			bootUpWaiter = new BootUpWaiter(factory, node_id);
		}

		#endregion Constructors

		#region Methods

		public void Dispose()
		{
			bootUpWaiter.Dispose();
		}

		public ApplicationStarter Start()
		{
			while (bootupMsgs.Count > 0)
			{
				bootupMsgs.Take();
			}

			CanOpenStatus status;

			status = clientSDO.connect(node_id);
			if (status != CanOpenStatus.CANOPEN_OK)
			{
				throw new IOException("SDO: не удалось подключиться к устройству");
			}

			status = clientSDO.objectWrite(start_app_index, start_app_subindex, start_code, out uint error);
			if (status != CanOpenStatus.CANOPEN_OK)
			{
				throw new IOException($"Не удалось записать значение {start_code} в индекс " +
					$"{start_app_index:X}sub{start_app_subindex} ({status.ToString()})");
			}

			return this;
		}

		public ApplicationStarter WaitBootup(int timeout_ms = 100)
		{
			bootUpWaiter.WaitBootup(timeout_ms);
			return this;
		}

		public ApplicationStarter Verify()
		{
			var res = clientSDO.objectRead(FWWriter.APPIdentityIndex, 1, out uint actualCRC, out _);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось прочитать CRC из устрйства", res);
			}
			if (actualCRC == 0)
			{
				throw new IndexOutOfRangeException("Загрузчик сообщает о некорректном образе приложения");
			}

			return this;
		}

		#endregion Methods
	}
}