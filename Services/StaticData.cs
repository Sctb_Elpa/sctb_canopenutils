﻿namespace Services
{
	public static class StaticData
	{
		#region Fields

		public static readonly uint sctbBootloaderProductId = 0xCAB1;
		public static readonly uint sctbVendorID = 0x4F038C;

		#endregion Fields
	}
}
