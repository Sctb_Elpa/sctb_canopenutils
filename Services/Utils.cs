﻿using DatalinkEngineering.CANopen;
using Services.LSSFastScan;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Tako.CRC;

namespace Services
{
	public static class Utils
	{
		#region Fields

		public static readonly ushort LSSIDIndex = 0x1018;

		public static readonly Dictionary<int, byte> speedMap = new Dictionary<int, byte>()
		{
			{10000, 8},
			{20000, 7},
			{50000, 6},
			{125000, 4},
			{250000, 3},
			{500000, 2},
			{800000, 1},
			{1000000, 0}
		};

		#endregion Fields

		#region Methods

		public static byte AssignNodeID(Factory factory, UniqueLSSId lssid, byte?
					node_id = null, int? speed = null)
		{
			if (!lssid.Valid())
			{
				var scaner = factory.getFastScaner();

				if (!scaner.FindDevicePartialyKnown(ref lssid, FastScanConsoleReporter.ScanProgressReport))
				{
					throw new CanError("Не удалось обнаружить устройств с задынным LSS ID",
						CanOpenStatus.CANOPEN_ARG_ERROR);
				}

				Console.WriteLine();
				Console.WriteLine($"Обнаружено соответсвие: {lssid.ToString()}");
			}
			else
			{
				var lssMaster = factory.getLSSMaster();

				Console.Write($"Переключение устройства {lssid.ToString()} в конфигурационный режим... ");

				var res = lssMaster.SetConfigurrationMode(lssid);
				if (res != CanOpenStatus.CANOPEN_OK)
				{
					throw new CanError($"Ошибка", res);
				}
				Console.WriteLine("Успешно");
			}

			Console.Write($"Установка идентификатора... ");

			byte nodeID;
			if (!node_id.HasValue)
			{
				nodeID = LSSAssignNodeID(factory, node_id);
			}
			else
			{
				nodeID = (byte)node_id;
			}

			Console.WriteLine(nodeID);
			
			if (speed.HasValue)
			{
				Console.Write($"Установка рабочей скорости {speed} B/s... ");

				LSSSetSpeed(factory, (int)speed);

				Console.WriteLine("Успешно");
			}

			storeLSSConfig(factory);

			if (speed.HasValue)
			{
				Console.Write($"Переключение скорости CAN... ");

				LSSActivateBitrate(factory, (int)speed);

				Console.WriteLine("Успешно");
			}

			factory.getLSSMaster().switchModeGlobal(0);

			return nodeID;
		}

		public static ushort crc16_ccit_0(byte[] data)
		{
			var mgr = new CRCManager();
			var crc16calc = mgr.CreateCRCProvider(EnumCRCProvider.CRC16CCITT_0x0000);

			var crc_res = crc16calc.GetCRC(data);
			var bytes = crc_res.CrcArray;

			return BitConverter.ToUInt16(bytes.Reverse().ToArray(), 0);
		}

		public static UniqueLSSId devReadLssID(Factory factory, byte nodeID)
		{
			var sdo_client = factory.getSDOClient();

			sdo_client.connect(nodeID);

			var res = new UniqueLSSId();

			CanOpenStatus err;

			err = sdo_client.objectRead(LSSIDIndex, 1, out res.VendorID, out uint _);
			if (err != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось прочитать Идентификатор произволителя", err);
			}

			err = sdo_client.objectRead(LSSIDIndex, 2, out res.ProductID, out uint _);
			if (err != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось прочитать Идентификатор произволителя", err);
			}

			err = sdo_client.objectRead(LSSIDIndex, 3, out res.RevisionNumber, out uint _);
			if (err != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось прочитать Идентификатор произволителя", err);
			}

			err = sdo_client.objectRead(LSSIDIndex, 4, out res.SerialNumber, out uint _);
			if (err != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось прочитать Идентификатор произволителя", err);
			}

			res.KnownMask = UniqueLSSId.KnownBits.All;

			return res;
		}

		public static bool IsBootloader(ClientSDO_NET clientSDO, byte node_id)
		{
			clientSDO.connect(node_id);
			var res = clientSDO.objectRead(0x1f51, 0, out byte subindex_count, out uint err);
			return res == CanOpenStatus.CANOPEN_OK && subindex_count == 1;
		}

		public static void LSSActivateBitrate(Factory factory, int bitrate)
		{
			var err = factory.getLSSMaster().activateBitTimingParameters(0);
			if (err != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Ошибка изменения скорости CAN", err);
			}

			Thread.Sleep(10);
			factory.Reset(factory.adapter_id, bitrate);
			Thread.Sleep(10);
		}

		public static byte LSSAssignNodeID(Factory factory, byte? node_id = null)
		{
			var nodeChecker = new NodeChecker(factory.getSDOClient());
			var lssMaster = factory.getLSSMaster();

			var nodeID = node_id.HasValue ? (byte)node_id : nodeChecker.NextUnusedNodeId(node_id);

			var c = lssMaster.configureNodeId(nodeID, out byte err, out byte sp_err_code);
			if (c != CanOpenStatus.CANOPEN_OK)
			{
				lssMaster.switchModeGlobal(0);
				throw new CanError($"Неудачная операция LSS", c);
			}

			return nodeID;
		}

		public static void LSSSetSpeed(Factory factory, int speed)
		{
			var lssMaster = factory.getLSSMaster();

			var c = lssMaster.configureBitTimingParamteres(0, speedMap[speed],
				out byte errCode, out byte specificeErr);
			if (c != CanOpenStatus.CANOPEN_OK)
			{
				lssMaster.switchModeGlobal(0);
				throw new CanError($"Неудачная операция LSS ({errCode})", c);
			}
		}

		// https://stackoverflow.com/a/28541237
		public static T Number<T>(this string mystring)
		{
			var foo = TypeDescriptor.GetConverter(typeof(T));
			return (T)(foo.ConvertFromInvariantString(mystring));
		}

		public static CanOpenStatus SetConfigurrationMode(this LSS_Master_NET master, UniqueLSSId id)
		{
			CanOpenStatus res;
			res = master.switchModeSelectiveVendorId(id.VendorID);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				return res;
			}

			master.switchModeSelectiveProductCode(id.ProductID);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				return res;
			}

			master.switchModeSelectiveRevisionNumber(id.RevisionNumber);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				return res;
			}

			return master.switchModeSelectiveSerialNumber(id.SerialNumber);
		}

		public static void storeLSSConfig(Factory factory)
		{
			var lssMaster = factory.getLSSMaster();

			var res = lssMaster.storeConfiguration(out byte err, out byte spErr);

			if (res != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось сохранить конфигурацию", res);
			}
		}

		public static void storeLSSConfigAndGoNormalMode(Factory factory)
		{
			var lssMaster = factory.getLSSMaster();

			var res = lssMaster.storeConfiguration(out byte err, out byte spErr);
			lssMaster.switchModeGlobal(0);

			if (res != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось сохранить конфигурацию", res);
			}
		}

		#endregion Methods
	}
}