﻿using Services.LSSFastScan;
using System;

namespace Services
{
	public static class FastScanConsoleReporter
	{
		#region Methods

		public static void ScanProgressReport(byte lss_bit_check, byte lss_sub)
		{
			string pattern;
			if (lss_bit_check == 0)
			{
				pattern = '*' + new string('.', 31);
			}
			else if (lss_bit_check == 31)
			{
				pattern = new string('.', lss_bit_check) + '*';
			}
			else
			{
				pattern = new string('.', lss_bit_check) + '*' + new string('.', 31 - lss_bit_check);
			}

			//Console.SetCursorPosition(0, Console.CursorTop);
			Console.Write($"\rПроверка битов в {UniqueLSSId.FieldName(lss_sub),14} : {pattern}");
		}

		#endregion Methods
	}
}