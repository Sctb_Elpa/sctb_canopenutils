﻿using System;

namespace Services
{
	public class ODIndex
	{
		#region Fields

		public ushort Index = 0;
		public byte Subindex = 0;

		#endregion Fields

		#region Constructors

		public ODIndex()
		{
		}

		public ODIndex(string str)
		{
			if (str.Contains("sub"))
			{
				var elements = str.Split(new string[] { "sub" }, StringSplitOptions.RemoveEmptyEntries);
				if (elements.Length != 2)
				{
					throw new ArgumentException("Index mast be XXXX[subYY] format");
				}

				Index = ushort.Parse(elements[0], System.Globalization.NumberStyles.HexNumber);
				Subindex = byte.Parse(elements[1]);
			}
			else
			{
				Index = ushort.Parse(str, System.Globalization.NumberStyles.HexNumber);
			}
		}

		#endregion Constructors

		#region Methods

		public override string ToString() => $"{Index:X}sub{Subindex}";

		#endregion Methods
	}
}