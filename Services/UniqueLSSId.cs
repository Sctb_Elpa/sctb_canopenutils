﻿using System;
using System.Collections.Generic;

namespace Services
{
	namespace LSSFastScan
	{
		public class UniqueLSSId
		{
			#region Fields

			public enum KnownBits
			{
				VID = 1 << 0,
				PID = 1 << 1,
				Revision = 1 << 2,
				Serial = 1 << 3,

				All = VID | PID | Revision | Serial
			}

			public uint ProductID = 0;
			public uint RevisionNumber = 0;
			public uint SerialNumber = 0;
			public uint VendorID = 0;

			public KnownBits KnownMask = 0;
			private static readonly string[] fields = { "VendorID", "ProductID", "RevisionNumber", "SerialNumber" };

			#endregion Fields

			#region Constructors

			public UniqueLSSId()
			{
			}

			public UniqueLSSId(IEnumerable<uint> lssIdentity)
			{
				var it = lssIdentity.GetEnumerator();

				if (it.MoveNext())
				{
					VendorID = it.Current;
					KnownMask |= KnownBits.VID;
				}
				if (it.MoveNext())
				{
					ProductID = it.Current;
					KnownMask |= KnownBits.PID;
				}
				if (it.MoveNext())
				{
					RevisionNumber = it.Current;
					KnownMask |= KnownBits.Revision;
				}
				if (it.MoveNext())
				{
					SerialNumber = it.Current;
					KnownMask |= KnownBits.Serial;
				}
			}

			public UniqueLSSId(IEnumerable<string> lssIdentity)
			{
				var it = lssIdentity.GetEnumerator();

				if (it.MoveNext())
				{
					try
					{
						VendorID = it.Current.Number<uint>();
						KnownMask |= KnownBits.VID;
					}
					catch (Exception) { }
				}
				if (it.MoveNext())
				{
					try
					{
						ProductID = it.Current.Number<uint>();
						KnownMask |= KnownBits.PID;
					}
					catch (Exception) { }
				}
				if (it.MoveNext())
				{
					try
					{
						RevisionNumber = it.Current.Number<uint>();
						KnownMask |= KnownBits.Revision;
					}
					catch (Exception) { }
				}
				if (it.MoveNext())
				{
					try
					{
						SerialNumber = it.Current.Number<uint>();
						KnownMask |= KnownBits.Serial;
					}
					catch (Exception) { }
				}
			}

			#endregion Constructors

			#region Indexers

			public uint this[byte component]
			{
				get
				{
					switch (component)
					{
						case 0:
							return VendorID;

						case 1:
							return ProductID;

						case 2:
							return RevisionNumber;

						case 3:
							return SerialNumber;

						default:
							throw new IndexOutOfRangeException("Component nimber mast be in range 0-3");
					}
				}
				set
				{
					switch (component)
					{
						case 0:
							VendorID = value;
							break;

						case 1:
							ProductID = value;
							break;

						case 2:
							RevisionNumber = value;
							break;

						case 3:
							SerialNumber = value;
							break;

						default:
							throw new IndexOutOfRangeException("Component nimber mast be in range 0-3");
					}
				}
			}

			#endregion Indexers

			#region Methods

			public static string FieldName(byte component) => fields[component];

			public bool IsSubKnown(byte lss_sub) => (KnownMask & (KnownBits)(1 << lss_sub)) > 0;

			public override string ToString()
			{
				var vid = ((KnownMask & KnownBits.VID) > 0)
					? $"0x{VendorID:X}"
					: "n/a";

				var pid = ((KnownMask & KnownBits.PID) > 0)
					? $"0x{ProductID:X}"
					: "n/a";

				var rev = ((KnownMask & KnownBits.Revision) > 0)
					? $"{RevisionNumber}"
					: "n/a";

				var ser = ((KnownMask & KnownBits.Serial) > 0)
					? $"{SerialNumber}"
					: "n/a";

				return $"Vendor: {vid}, Product: {pid}, Revision: {rev}, Serial: {ser}";
			}

			public bool Valid() => KnownMask == KnownBits.All
				&& VendorID > 0 && ProductID > 0 && SerialNumber > 0;

			#endregion Methods
		}
	}
}