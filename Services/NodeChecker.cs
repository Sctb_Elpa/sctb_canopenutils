﻿using DatalinkEngineering.CANopen;
using System;

namespace Services
{
	public class NodeChecker
	{
		#region Fields

		private ClientSDO_NET clientSDO;
		private byte NodeId = 1;

		#endregion Fields

		#region Constructors

		public NodeChecker(ClientSDO_NET clientSDO)
		{
			this.clientSDO = clientSDO;
		}

		#endregion Constructors

		#region Methods

		public byte NextUnusedNodeId(byte? node_id = null)
		{
			if (node_id.HasValue)
			{
				NodeId = (byte)node_id;
			}

			while (NodeId < 127)
			{
				clientSDO.connect(NodeId);
				var res = clientSDO.objectRead(0x1000, 0, out uint val, out uint error);
				if (res != CanOpenStatus.CANOPEN_OK)
				{
					return NodeId;
				}

				++NodeId;
			}
			throw new IndexOutOfRangeException("No free NodeID's avalable!");
		}

		#endregion Methods
	}
}