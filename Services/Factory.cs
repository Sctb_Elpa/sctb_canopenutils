﻿using DatalinkEngineering.CANopen;
using Services.LSSFastScan;
using System.IO;

namespace Services
{
	public class Factory
	{
		#region Fields

		private ClientSDO_NET clientSDO = null;
		private LSS_Master_NET lSS_Master = null;
		private LSSFastScanScaner lSSFastScanScaner = null;
		private NMT_Master_NET nMT_Master = null;

		#endregion Fields

		#region Constructors

		public Factory()
		{
		}

		public Factory(int adapter_id, int CanBitrate)
		{
			this.adapter_id = adapter_id;
			this.CanBitrate = CanBitrate;
		}

		#endregion Constructors

		#region Properties

		public int adapter_id { get; private set; } = 0;
		public int CanBitrate { get; private set; } = 50000;

		#endregion Properties

		#region Methods

		private static void ThrowConnectError(CanOpenStatus code)
					=> throw new CanError("Не удаётся открыть адаптер", code);

		public NMT_Master_NET buildNMTMaster()
		{
			var res = new NMT_Master_NET();

			var c = res.canHardwareConnect(adapter_id, CanBitrate);
			if (c != CanOpenStatus.CANOPEN_OK)
			{
				ThrowConnectError(c);
			}

			return res;
		}

		public LSSFastScanScaner getFastScaner()
		{
			if (lSSFastScanScaner != null)
			{
				return lSSFastScanScaner;
			}

			var res = new LSSFastScanScaner();

			var c = res.canHardwareConnect(adapter_id, CanBitrate);
			if (c != CanOpenStatus.CANOPEN_OK)
			{
				ThrowConnectError(c);
			}
			lSSFastScanScaner = res;

			return res;
		}

		public LSS_Master_NET getLSSMaster()
		{
			if (lSS_Master != null)
			{
				return lSS_Master;
			}

			var res = new LSS_Master_NET();

			var c = res.canHardwareConnect(adapter_id, CanBitrate);
			if (c != CanOpenStatus.CANOPEN_OK)
			{
				ThrowConnectError(c);
			}
			lSS_Master = res;

			return res;
		}

		public NMT_Master_NET getNMTMaster()
		{
			if (nMT_Master != null)
			{
				return nMT_Master;
			}

			var res = buildNMTMaster();
			nMT_Master = res;
			return res;
		}

		public ClientSDO_NET getSDOClient()
		{
			if (clientSDO != null)
			{
				return clientSDO;
			}

			var res = new ClientSDO_NET();

			var c = res.canHardwareConnect(adapter_id, CanBitrate);
			if (c != CanOpenStatus.CANOPEN_OK)
			{
				ThrowConnectError(c);
			}
			clientSDO = res;

			return res;
		}

		public void Reset(int adapter_id, int CanBitrate)
		{
			if (clientSDO != null)
			{
				clientSDO.Dispose();
				clientSDO = null;
			}

			if (lSS_Master != null)
			{
				lSS_Master.Dispose();
				lSS_Master = null;
			}

			if (lSSFastScanScaner != null) {
				lSSFastScanScaner.Dispose();
				lSSFastScanScaner = null;
			}

			if (nMT_Master != null)
			{
				nMT_Master.Dispose();
				nMT_Master = null;
			}

			this.adapter_id = adapter_id;
			this.CanBitrate = CanBitrate;
		}

		#endregion Methods
	}

	public class CanError : IOException
	{
		#region Fields

		public CanOpenStatus code { get; private set; }

		#endregion Fields

		#region Constructors

		public CanError(string text, CanOpenStatus code = CanOpenStatus.CANOPEN_ERROR) : base(text)
		{
			this.code = code;
		}

		#endregion Constructors

		#region Properties

		public override string Message => base.Message + $" ({code})";

		#endregion Properties
	}
}