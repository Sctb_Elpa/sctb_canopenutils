﻿using DatalinkEngineering.CANopen;
using System;

namespace Services
{
	public class BootloaderStarter : IDisposable
	{
		private byte nodeID;
		private NMT_Master_NET nmt_master;
		private BootUpWaiter bootUpWaiter;

		public BootloaderStarter(Factory factory, byte nodeID)
		{
			nmt_master = factory.getNMTMaster();
			bootUpWaiter = new BootUpWaiter(factory, nodeID);

			this.nodeID = nodeID;
		}

		public void Dispose() => nmt_master.Dispose();

		public BootloaderStarter Start()
		{
			var res = nmt_master.nodeStop(nodeID);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось Остановить устройство", res);
			}

			return this;
		}

		public BootloaderStarter WaitBootup(int timeout_ms = 100)
		{
			bootUpWaiter.WaitBootup(timeout_ms);
			return this;
		}
	}
}
