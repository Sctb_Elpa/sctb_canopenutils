﻿using DatalinkEngineering.CANopen;
using System;

namespace Services
{
	public class FWWriter
	{
		#region Fields

		public static readonly ushort APPIdentityIndex = 0x1f56;
		public static readonly ushort ApplicationDataIndex = 0x1f50;
		public static readonly ushort EncryptionSelectorIndex = 0x1f55;

		private ClientSDO_NET clientSDO;

		#endregion Fields

		#region Constructors

		public FWWriter(ClientSDO_NET clientSDO)
		{
			this.clientSDO = clientSDO;
		}

		#endregion Constructors

		#region Methods

		public uint Verify(byte[] firmware)
		{
			var res = clientSDO.objectRead(APPIdentityIndex, 1, out uint actualCRC, out _);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось прочитать CRC из устрйства", res);
			}

			var crc = GetImage_crc(firmware);
			if (crc != actualCRC)
			{
				if (actualCRC == 0)
				{
					throw new CanError("Верификация образа приложения на устройстве провалена");
				}
				else
				{
					throw new CanError($"Верификация образа приложения провалена: ожидается {crc:X}, получено {actualCRC:X}");
				}
			}

			return actualCRC;
		}

		public FWWriter Write(byte[] firmware, bool encrypted)
		{
			CanOpenStatus res;

			res = clientSDO.objectWrite(EncryptionSelectorIndex, 1, encrypted ? 1u : 0u, out _);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError("Не удалось установить метод шифрования", res);
			}

			var crc = Utils.crc16_ccit_0(firmware);

			//res = clientSDO.objectWrite(ApplicationDataIndex, 1, firmware, (uint)firmware.Length, out uint err);
			res = clientSDO.objectWriteBlock(ApplicationDataIndex, 1, crc, firmware, (uint)firmware.Length, out uint err);
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				throw new CanError($"Не удалось загрузить образ ({err:X})", res);
			}

			return this;
		}

		private uint GetImage_crc(byte[] firmware) => BitConverter.ToUInt32(firmware, 6 * sizeof(uint));

		#endregion Methods
	}
}