﻿using CommandLine;
using DatalinkEngineering.CANopen;
using Services;
using Services.LSSFastScan;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CANKickStart
{
	internal class Options
	{
		[Option('p', "port", Default = 0, Required = false, HelpText = "Adapter number")]
		public int Port { get; set; }

		[Option('b', "bitrate", Default = 50000, Required = false, HelpText = "CAN Bitrate")]
		public int Bitrate { get; set; }

		[Option("lss", Required = false, Separator = ':', SetName = "lssAdress", HelpText = "Specify LSS identity (VID:PID:REVISION:SERIAL)")]
		public IEnumerable<string> LssIdentity { get; set; }

		[Option('N', "nodeid", Required = false, SetName = "canAdress", HelpText = "Specify NodeID")]
		public byte? NodeID { get; set; }
	}

	internal class Program
	{
		#region Methods

		private static int Main(string[] args)
		{
			return Parser.Default.ParseArguments<Options>(args)
				.MapResult(
				  options => RunAndReturnExitCode(options),
				  _ => 1);
		}

		private static int RunAndReturnExitCode(Options options)
		{
			CanOpenStatus status;

			var factory = new Services.Factory(options.Port, options.Bitrate);

			LSSFastScanScaner scaner;
			LSS_Master_NET lssMaster;
			ClientSDO_NET clientSDO;

			Console.WriteLine($"Открывается адаптер {options.Port}...");

			try
			{
				scaner = factory.getFastScaner();
				lssMaster = factory.getLSSMaster();
				clientSDO = factory.getSDOClient();
			}
			catch (Services.CanError ex)
			{
				Console.WriteLine(ex.Message);
				return 1;
			}

			if (options.NodeID.HasValue)
			{
				return StartApp(factory, (byte)options.NodeID);
			}
			else if (options.LssIdentity.Count() > 0)
			{
				var lssid = new UniqueLSSId(options.LssIdentity);

				Console.WriteLine($"Попытка обнаружить устройство с lSS ID {lssid}...");

				byte nodeID = 0;
				try
				{
					nodeID = Utils.AssignNodeID(factory, lssid);
				}
				catch (CanError ex)
				{
					Console.WriteLine();
					Console.WriteLine(ex.Message);
					return 1;
				}

				return StartApp(factory, nodeID);
			}
			else
			{
				bool any_dev_found = false;
				while (true)
				{
					Console.WriteLine($"Поиск ненастроенных устройств...");

					byte nodeID;
					try
					{
						nodeID = Utils.AssignNodeID(factory, new UniqueLSSId());
					}
					catch (CanError ex)
					{
						if (ex.code == CanOpenStatus.CANOPEN_ARG_ERROR)
						{
							string more = any_dev_found ? "больше " : "";
							Console.WriteLine($"Ненастроенных устройств {more}не найдено, выход.");
							return 0;
						}
						else
						{
							Console.WriteLine();
							Console.WriteLine(ex.Message);
							return 1;
						}
					}

					any_dev_found = true;

					StartApp(factory, nodeID);

					separator();
				}
			}
		}

		private static int StartApp(Factory factory, byte nodeID)
		{
			Console.WriteLine($"Проверка устройства NodeID {nodeID}...");

			var clientSDO = factory.getSDOClient();

			// check if device bootloader
			if (Utils.IsBootloader(clientSDO, nodeID))
			{
				Console.Write("Устройство в режиме загрузчика, запуск приложения... ");

				// start application
				using (var applicationStartter = new ApplicationStarter(clientSDO, factory, nodeID))
				{
					try
					{
						// wait bootup
						applicationStartter.Verify().Start().WaitBootup(500);
					}
					catch (Exception ex)
					{
						Console.WriteLine("Ошибка!");
						Console.WriteLine($"Не удалось запустить приложение! ({ex.Message})");
						separator();
						return 1;
					}
					Console.WriteLine("Успешно!");
				}
			}
			else
			{
				Console.WriteLine("Устройство в режиме приложения.");
			}

			return 0;
		}

		private static void separator()
		{
			Console.WriteLine("-----");
		}

		#endregion Methods
	}
}