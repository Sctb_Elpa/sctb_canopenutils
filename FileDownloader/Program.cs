﻿using CommandLine;
using DatalinkEngineering.CANopen;
using Services;
using Services.LSSFastScan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FirmwareUpdater
{
	internal class Options
	{
		// adapter number
		[Option('p', "port", Default = 0, Required = false, HelpText = "Adapter number")]
		public int Port { get; set; }

		// can bitrate
		[Option('b', "bitrate", Default = 50000, Required = false, HelpText = "CAN Bitrate")]
		public int Bitrate { get; set; }

		[Option('N', "nodeid", Required = true, SetName = "canAdress", HelpText = "NodeID to connect to")]
		public byte NodeID { get; set; }

		[Value(0, Required = true, MetaName = "Index", HelpText = "Index to read XXXXsubYY")]
		public string Index { get; set; }
	}

	internal class Program
	{
		private static int Main(string[] args)
		{
			return Parser.Default.ParseArguments<Options>(args)
					.MapResult(
					  options => RunAndReturnExitCode(options),
					  _ => 1);
		}

		private static int RunAndReturnExitCode(Options options)
		{
			var index = new ODIndex(options.Index);

			var Factory = new Factory(options.Port, options.Bitrate);

			var sdoclient = Factory.getSDOClient();

			byte[] result = new byte[1024*8192];

			sdoclient.connect(options.NodeID);
			var res = sdoclient.objectRead(index.Index, index.Subindex, result, out uint size, out uint err);
			
			if (res != CanOpenStatus.CANOPEN_OK)
			{
				Console.WriteLine($"Can't read index {index} form device id {options.NodeID} ({res} ({err}))");
				return 1;
			}
			
			using (var stdo = Console.OpenStandardOutput())
			{
				stdo.Write(result, 0, (int)size);
			}

			return 0;
		}
	}
}