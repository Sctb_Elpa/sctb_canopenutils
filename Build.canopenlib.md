﻿# Build.canopenlib.md

Библиотека состоит из трех основных компонентов:

1. Ядро, отвечающее за всю логику работы протокола CANOpen и све его службы.
2. Драйвер адаптера шины CAN для ПК (в виде плагина .dll)
3. Мост, пробрасывающий API ядра в C#

Все три модуля выделены как отдельные проекты, каждый из которых собирается в отдельную dll
Из всего набора драйверов, поддерживаемых библиотекой надлежит выбрать один, который должен быть собран
в библиотеку canopenlib32_hw.dll

Поскольку система сборки Visual Studio недостаточно гибкая, чтобы управлять зависимостями между проектами С++ и C#,
приходится руками копировать результат сборки в каталог к исполняемому файлу.

## Порядок сборки

0. Этот репозиторий склонирован куда-либо
1. Клонирование субмодулей

`$ git submodule update --init --recursive`

Появится каталог canopenlib

2. Скачать исходники драйвера адаптера Candle.

```
$ cd canopenlib/canopen_stack_visual_studio_2008-2012/visual_studio
$ git clone --recurse-submodules https://github.com/ololoshka2871/canopenlib-Candle.git plug_in_candle
```

3. Открыть проект SCTB_CANOpenUtils.sln в MS Visual Studio (проверено в MSVS 2019)

_Согласиться на обновление версии SDK_

4. Выбрать режим сборки проекта - *x86*

5. Собрать проекты `canopenlib32.dll` и `canopenlib32_hw.dll (Candle)` вручную.

canopenlib32_NET.dll (C++ to C Sharp wrapper) Собирать не нужно, он соберется по зависимостям.

6. Двоичные файлы .dll собранных в п. 4 проектов надлежит скопировать в каталог с исполняемым файлом.

## Продвинутый уровень. Дечение от жадности.

Пока приложение, использующее canopenlib32 запущено а либа не зарегана будет рандомно выскакивать окошко
с предупреждением. Чтобы его выпилить необходимо проделать следующие действия с canopenlib32.dll

Используя IDA pro

1. Загрузить на анализ canopenlib32.dll
2. Найти функцию `canopenDispatcher(void*)`
3. Переключиться на вкладку Hex View-1
4. найти последовательность 
	`ce 50 e8 49 29 20 20 85 c0 75 5c a1 44 3b 0b 10`
	Она должна быть рядом с вызоввом метода `CanInterface::canGetSerialNumber(...)`
	Заменить байты 75 56 на 90 90
	`ce 50 e8 49 29 20 20 85 c0 90 90 a1 44 3b 0b 10`
5. Найти последовательность
	`44 24 17 84 c0 74 2c 6a 20 68 78 3b 0b 10 68 a0`
	Она должна быть после вызова `ValidateLicenseFile(...)`
	Заменить 74 на EB
	`44 24 17 84 c0 EB 2c 6a 20 68 78 3b 0b 10 68 a0`
Суть в том, чтобы отключить вывод диалогового окна, а для этого заменяем coditiaonal-jump'ы на `NOP` или простые `JMP`