﻿using CommandLine;
using Services;
using Services.LSSFastScan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FirmwareUpdater
{
	internal class Options
	{
		// adapter number
		[Option('p', "port", Default = 0, Required = false, HelpText = "Adapter number")]
		public int Port { get; set; }

		// can bitrate
		[Option('b', "bitrate", Default = 50000, Required = false, HelpText = "CAN Bitrate")]
		public int Bitrate { get; set; }

		// lss address OR NodeID OR auto
		[Option("lss", Required = false, Separator = ':', SetName = "lssAdress", HelpText = "Specify LSS identity (VID:PID:REVISION:SERIAL)")]
		public IEnumerable<string> LssIdentity { get; set; }

		[Option('N', "nodeid", Required = false, SetName = "canAdress", HelpText = "Specify NodeID")]
		public byte? NodeID { get; set; }

		// isEncrypted
		[Option('E', "encrypted", Default = false, HelpText = "Upload an encrypted application image")]
		public bool? Encrypted { get; set; }

		// overide upload speed
		[Option('S', "upload-speed", HelpText = "Overide opload speed")]
		public int? Uploadspeed { get; set; }

		// settings restore
		[Option("restore-settings", Default = true, HelpText = "Backup and restore applicatio settings")]
		public bool? RestoreSettings { get; set; }

		// file
		[Value(0, Required = true, MetaName = "AppImage", HelpText = "Application image to flash")]
		public string File { get; set; }
	}

	internal class Program
	{
		private static int Main(string[] args)
		{
			Array.ForEach(args, v => Console.Write($"{v} "));
			Console.WriteLine();

			return Parser.Default.ParseArguments<Options>(args)
					.MapResult(
					  options => RunAndReturnExitCode(options),
					  _ => 1);
		}

		private static byte[] loadFirmware(string fname)
		{
			Console.Write($"Загрузка обновления из файла {fname}... ");
			try
			{
				var data = File.ReadAllBytes(fname);
				Console.WriteLine($"Успешно! (Размер: {data.Length} байт)");
				return data;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Ошибка: {ex.Message}");
				Environment.Exit(1);
				return null;
			}
		}

		private static int UpdateFirmware(Factory factory, byte nodeID, byte[] firmware, bool encrypted, bool restoreSettings)
		{
			Console.WriteLine($"Обновление приложения в устройстве c ID: {nodeID}...");

			var clientSDO = factory.getSDOClient();

			clientSDO.connect(nodeID);

			// check is bootloader
			if (!Utils.IsBootloader(clientSDO, nodeID))
			{
				Console.Write("Устройство в режиме приложения, переход в загрузчик... ");

				// go to bootloader if needed
				using (var bootloaderStarter = new BootloaderStarter(factory, nodeID))
				{
					try
					{
						// wait bootup
						bootloaderStarter.Start().WaitBootup(500);
					}
					catch (Exception ex)
					{
						Console.WriteLine("Ошибка!");
						Console.WriteLine($"Не удалось запустить загрузчик! ({ex.Message})");
						return 1;
					}
					Console.WriteLine("Успешно!");
				}
			}
			else
			{
				Console.WriteLine("Устройство в режиме в режиме загрузчика.");
			}

			byte[] settings = null;
			BootloaderSettingsAccessor settingsAccessor = null;
			if (restoreSettings)
			{
				Console.Write("Чтение настроек из устройства... ");

				settingsAccessor = new BootloaderSettingsAccessor(clientSDO);
				try
				{
					settings = settingsAccessor.Load();
					Console.WriteLine($"успешно ({settings.Length} байт)");
				}
				catch (CanError ex)
				{
					Console.WriteLine(ex);
					return 1;
				}
			}

			var fwWriter = new FWWriter(clientSDO);

			Console.WriteLine("Загрузка образа приложания в устройство...");

			try
			{
				var crc = fwWriter.Write(firmware, encrypted).Verify(firmware);

				Console.WriteLine($"Приложение успешно обновлено, CRC=0x{crc:X}.");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return 1;
			}

			if (restoreSettings)
			{
				Console.Write("Восстановлеине настроек... ");

				try
				{
					settingsAccessor.Save(settings);
					Console.WriteLine($"успешно");
				}
				catch (CanError ex)
				{
					Console.WriteLine(ex);
					return 1;
				}
			}

			return 0; // success
		}

		private static int RunAndReturnExitCode(Options options)
		{
			var factory = new Factory(options.Port, options.Bitrate);

			var firmware = loadFirmware(options.File);

			var encrypted = true.Equals(options.Encrypted);
			{
				var encr = encrypted ? "зашифрован" : "не зашифрован";
				Console.WriteLine($"Образ {encr}");
			}

			var restoreSettings = true.Equals(options.RestoreSettings);
			{
				var rest = restoreSettings ? "да" : "нет";
				Console.WriteLine($"Восстановление настроек: {rest}");
			}

			if (options.NodeID.HasValue)
			{
				UniqueLSSId id = Utils.devReadLssID(factory, (byte)options.NodeID);
				if (options.Uploadspeed.HasValue)
				{
					options.NodeID = Utils.AssignNodeID(factory, id, options.NodeID, options.Uploadspeed);
				}

				var ret = UpdateFirmware(factory, (byte)options.NodeID, firmware, encrypted, restoreSettings);
				if (ret != 0 || !options.Uploadspeed.HasValue)
				{
					return ret;
				}

				return SetCanSpeed(factory, id, options.Bitrate);
			}
			else if (options.LssIdentity.Count() > 0)
			{
				var id = new UniqueLSSId(options.LssIdentity);

				Console.WriteLine($"Обновление приложения в устройстве c LSS ID соотвтетвующему шаблону: {id}...");

				byte nodeID;
				try
				{
					nodeID = Utils.AssignNodeID(factory, id, null, options.Uploadspeed);
				}
				catch (CanError ex)
				{
					Console.WriteLine();
					Console.WriteLine(ex.Message);
					return 1;
				}

				var ret = UpdateFirmware(factory, nodeID, firmware, encrypted, restoreSettings);
				if (ret != 0 || !options.Uploadspeed.HasValue)
				{
					return ret;
				}
				return SetCanSpeed(factory, id, options.Bitrate);
			}
			else
			{
				Console.WriteLine("Автоматическое обновление первого найденного стройства...");

				var id = FindFirstUnconfiguredDevice(factory);
				if (id == null)
				{
					Console.WriteLine("Ненастроенных устройств не найдено!");
					return 1;
				}

				Console.WriteLine();

				if (id.IsSCTBDevice())
				{
					Console.WriteLine($"Найдено устройство: {id.ToString()}");

					byte nodeID;
					try
					{
						nodeID = Utils.LSSAssignNodeID(factory);

						if (options.Uploadspeed.HasValue)
						{
							Utils.LSSSetSpeed(factory, (int)options.Uploadspeed);
						}

						Utils.storeLSSConfig(factory);

						if (options.Uploadspeed.HasValue)
						{
							Utils.LSSActivateBitrate(factory, (int)options.Uploadspeed);
						}

						factory.getLSSMaster().switchModeGlobal(0);
					}
					catch (CanError ex)
					{
						Console.WriteLine(ex.Message);
						return 1;
					}

					Console.WriteLine($"Успешно настроен узел {nodeID}...");

					var ret = UpdateFirmware(factory, nodeID, firmware, encrypted, restoreSettings);
					if (ret != 0 || !options.Uploadspeed.HasValue)
					{
						return ret;
					}
					return SetCanSpeed(factory, id, options.Bitrate);
				}
				else
				{
					factory.getLSSMaster().switchModeGlobal(0);
					Console.WriteLine($"Обнаружено неизвестное устройство {id.ToString()}! Обновление отменено.");
					Console.WriteLine("Пожалуйста отключите устройства сторонних производителей " +
						"или используйте конкретный NodeID или LSS ID");
					return 1;
				}
			}
		}

		private static int SetCanSpeed(Factory factory, UniqueLSSId id, int bitrate)
		{
			Console.Write($"Восстановление скорости шины {bitrate}... ");
			try
			{
				var lssMaster = factory.getLSSMaster();
				lssMaster.SetConfigurrationMode(id);
				Utils.LSSSetSpeed(factory, bitrate);
				Utils.storeLSSConfig(factory);
				Utils.LSSActivateBitrate(factory, bitrate);
				lssMaster = factory.getLSSMaster();
				lssMaster.switchModeGlobal(0);
			}
			catch (CanError ex)
			{
				Console.WriteLine(ex.Message);
				return 1;
			}

			Console.WriteLine("Успешно");

			return 0;
		}

		private static UniqueLSSId FindFirstUnconfiguredDevice(Factory factory)
		{
			var scaner = factory.getFastScaner();

			if (!scaner.FindDevice(out UniqueLSSId lssID, FastScanConsoleReporter.ScanProgressReport))
			{
				return null;
			}

			return lssID;
		}
	}

	internal static class Extantions
	{
		private static readonly uint sctbVendorID = 0x4F038C;

		internal static bool IsSCTBDevice(this UniqueLSSId id)
		{
			return id.VendorID == sctbVendorID;
		}
	}
}